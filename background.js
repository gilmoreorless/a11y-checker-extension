chrome.browserAction.onClicked.addListener(function () {
    chrome.tabs.getCurrent(function (tabId) {
        chrome.tabs.executeScript(tabId, {
            code: '(' + showTabOrder.toString() + ')()'
        });
    });
});

function showTabOrder() {
    var canvasId = 'tabindex-checker';
    var oldCanvas = document.getElementById(canvasId);
    if (oldCanvas) {
        oldCanvas.remove();
        return;
    }

    var selector = 'a,input:not([type=hidden]),textarea,select,button,[tabindex]';
    var tabElems = Array.prototype.slice.call(document.querySelectorAll(selector));
    console.log('Items:', tabElems.length);

    var tiZero = [];
    var tiPositive = [];
    tabElems.forEach(function (elem) {
        var tabIndex = elem.getAttribute('tabindex');
        var tiNum;
        if (tabIndex) {
            tiNum = +tabIndex || 0;
            if (tiNum > 0) {
                tiPositive.push({
                    elem: elem,
                    order: tiNum
                });
            } else if (tiNum === 0) {
                tiZero.push(elem);
            }
        } else {
            tiZero.push(elem);
        }
    });
    tiPositive.sort(function (a, b) {
        return a.order - b.order;
    });
    console.log('tabindex > 0:', tiPositive);
    console.log('tabindex == 0:', tiZero.length);

    var navigable = tiPositive
        .map(function (data) { return data.elem; })
        .concat(tiZero)
        .filter(function excludeWhenPossible(elem) {
            if (elem.disabled) {
                return false;
            }
            if (elem.nodeName === 'A' && elem.getAttribute('href') === null && elem.getAttribute('tabindex') === null) {
                return false;
            }
            var computed = getComputedStyle(elem);
            if (computed.display === 'none' || computed.visibility === 'hidden') {
                return false;
            }
            if (elem.offsetWidth === 0 || elem.offsetHeight === 0) {
                return elem.parentNode ? excludeWhenPossible(elem.parentNode) : false;
            }
            return true;
        });
//     navigable.forEach(function (elem, i) {
//         elem.style.outline = '1px solid hsl(' + (i * 5) + ',50%,50%)';
//         elem.style.outline = '';
//     });
//     console.log('navigable', navigable);
    var dims = navigable.map(function (elem) {
        var rect = elem.getBoundingClientRect();
        return {
            left: rect.left + document.documentElement.scrollLeft,
            top: rect.top + document.documentElement.scrollTop,
            width: rect.width,
            height: rect.height
        };
    });
    var docSize = document.documentElement.getBoundingClientRect();
    var bodySize = document.body.getBoundingClientRect();
    var canvas = document.createElement('canvas');
    canvas.width = Math.max(docSize.width, bodySize.width);
    canvas.height = Math.max(docSize.height, bodySize.height);
    canvas.id = canvasId;
    canvas.setAttribute('style', 'position:absolute;top:0;left:0;pointer-events:none;z-index:100000000');
    document.body.appendChild(canvas);
    var ctx = canvas.getContext('2d');
    var lastPos;
    dims.forEach(function (dim, i) {
        var radius = 10;
        var pos = {
            left: dim.left + dim.width / 2,
            top: dim.top + dim.height / 2
        };
        ctx.fillStyle = 'hsla(' + (i * 5) + ',50%,50%,0.2)';
        ctx.fillRect(dim.left, dim.top, dim.width, dim.height);
        ctx.fillStyle = 'hsla(' + (i * 5) + ',40%,10%,0.3)';
        ctx.beginPath();
        ctx.arc(pos.left, pos.top, radius, 0, Math.PI * 2, true);
        ctx.fill();
        if (lastPos) {
//             ctx.strokeStyle = 'rgba(0,0,0,0.3)';
//             ctx.strokeWidth = 1;
//             ctx.beginPath();
//             ctx.moveTo(lastPos.left, lastPos.top);
//             ctx.lineTo(pos.left, pos.top);
//             ctx.stroke();
            var w = pos.left - lastPos.left;
            var h = pos.top - lastPos.top;
            // Pythagoras FTW
            var d = Math.sqrt(w * w + h * h);
            var rot = -Math.atan2(w, h);

            ctx.save();
            ctx.fillStyle = 'hsla(' + (i * 5) + ',80%,30%,0.2)';
            ctx.translate(lastPos.left, lastPos.top);
            ctx.rotate(rot);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.lineTo(-radius / 2, d);
            ctx.arc(0, d, radius / 2, 0, Math.PI);
            ctx.lineTo(radius / 2, d);
            ctx.closePath();
            ctx.fill();
            ctx.restore();
        }
        lastPos = pos;
    });
}